//////////////////////////////// BY CACTUSFLUO /////////////////////////////////

"use strict";

window.browser = window.browser || window.chrome || window.msBrowser;

////////////////////////////////////////////////////////////////////////////////
/// DATA
////////////////////////////////////////////////////////////////////////////////

let	g_in_site		= null;
let	g_in_filetype	= null;
let	g_in_contains	= null;
let	g_in_search		= null;

////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

function		go_to_search() {
	let			url;

	url = "";
	/// SITE SPECIFIED
	if (g_in_site.value.length > 0) {
		url = `site:${g_in_site.value} `;
	}
	/// CONTAINS
	if (g_in_contains.value.length > 0) {
		url += `contains:${g_in_contains.value} `;
	}
	/// FILETYPE
	if (g_in_filetype.value.length > 0) {
		url += `filetype:${g_in_filetype.value} `;
	}
	/// SEARCH
	if (g_in_search.value.length > 0) {
		url += g_in_search.value;
	}
	url = `https://www.ecosia.org/search?q=${encodeURIComponent(url)}`
	browser.tabs.create({ "url": url });
}

function		go_to_ecosia() {
	browser.tabs.create({ "url": "https://ecosia.org" });
}

document.addEventListener('DOMContentLoaded', function() {
	let	b_search;
	let	img_ecosia;

	/// GET ELEMENTS
	img_ecosia = document.getElementById("ecosia");
	b_search = document.getElementById("b_search");
	g_in_site = document.getElementById("in_site");
	g_in_filetype = document.getElementById("in_filetype");
	g_in_contains = document.getElementById("in_contains");
	g_in_search = document.getElementById("in_search");
	/// OPEN MAIN PAGE WITH BUTTON
	b_search.addEventListener("click", go_to_search);
	img_ecosia.addEventListener("click", go_to_ecosia);
});
